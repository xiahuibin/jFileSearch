package net.qqxh.service.impl;

import net.qqxh.common.utils.FileAnalysisTool;
import net.qqxh.persistent.Jfile;
import net.qqxh.persistent.SearchLib;
import net.qqxh.persistent.h2.JfileRepository;
import net.qqxh.resolve2text.File2TextResolve;
import net.qqxh.resolve2text.File2TextResolveFactory;
import net.qqxh.resolve2text.exception.File2TextResolveException;
import net.qqxh.resolve2text.exception.File2TextResolveNotFondException;
import net.qqxh.resolve2view.File2ViewResolve;
import net.qqxh.resolve2view.File2ViewResolveFactory;
import net.qqxh.resolve2view.exception.File2ViewResolveException;
import net.qqxh.resolve2view.exception.File2ViewResolveNotFondException;
import net.qqxh.service.FileEsService;
import net.qqxh.service.FileResolveService;
import net.qqxh.service.task.FileResolveTask;
import org.apache.commons.io.FileUtils;
import org.jodconverter.office.OfficeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Service
public class FileResolveServiceImpl implements FileResolveService {
    private final static Logger logger = LoggerFactory.getLogger(FileResolveTask.class);
    @Autowired
    private File2TextResolveFactory file2TextResolveFactory;
    @Autowired
    private File2ViewResolveFactory file2ViewResolveFactory;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private FileEsService fileEsService;


    private static String FILE_RESOLVED_2_TEXT = "1";
    private static String FILE_RESOLVED_2_VIEW = "2";
    private static String FILE_RESOLVED_2_SHOW = "3";

    @Override
    public String resolveFile2View(String filePath, SearchLib searchLib, String type) throws IOException, OfficeException {
        File2ViewResolve file2ViewResolve = null;

        try {
            file2ViewResolve = file2ViewResolveFactory.getFile2ViewResolve(type);
        } catch (File2ViewResolveNotFondException e) {
            e.printStackTrace();
            return null;
        }
        String toPath = FileAnalysisTool.getOffSiteViewDir(searchLib.getFileSourceDir(), searchLib.getFileViewDir(), filePath, file2ViewResolve.getviewFix());
        return file2ViewResolve.resolve(filePath, toPath);
    }

    @Override
    public String resolveFile2Text(File file, String type) throws IOException {
        File2TextResolve file2TextResolve = null;
        try {
            file2TextResolve = file2TextResolveFactory.getFileResolve(type);
        } catch (File2TextResolveNotFondException e) {
            e.printStackTrace();
            return null;
        }
        return file2TextResolve.resolve(file);
    }

    @Override
    public void deleteFileFromRedis(String rid) {
        stringRedisTemplate.delete(rid);
    }

    public Jfile findFileByPath(String path) {

        return null;
    }

    @Override
    public boolean resolve(SearchLib searchLib, Jfile jfile) throws IOException, OfficeException {
        String path = jfile.getPath();
        String fileName = jfile.getName();
        String suffix = jfile.getFix();
        File file = new File(path);

        String content = "";
        /*转换为检索文本*/
        if (file.exists()) {
            try {
                content = resolveFile2Text(file, suffix);
            } catch (Exception e) {
                logger.error("获取文件内容异常,{}", e.fillInStackTrace());
                throw new File2TextResolveException("获取文件内容异常");
            }

        }
        /*插入之前先删除*/
        List<Jfile> list = fileEsService.findFileByPath(searchLib, path);
        for (Jfile oldJfile : list) {
            logger.warn("发现老文件数据正在清理中，文件路径：" + path);
            fileEsService.deleteFileFromEs(searchLib.getEsIndex(), oldJfile.getRid());
        }
        /*支持查看*/
        String viewPath;
        try {
            viewPath = resolveFile2View(path, searchLib, suffix);
        } catch (Exception e) {
            logger.error("视图转换异常,{}", e.fillInStackTrace());
            throw new File2ViewResolveException();
        };

        String viewFix = "";
        if (!viewPath.equals(path)) {
            viewFix = FileAnalysisTool.getFileFix(viewPath);
        }
        /*支持检索*/
        fileEsService.addFile2ES(searchLib.getEsIndex(), content, fileName, path, viewFix);
        return true;
    }


}
