package net.qqxh.handler;

import net.qqxh.persistent.Jfile;
import net.qqxh.persistent.SearchLib;
import net.qqxh.service.task.FileResolveTask;
import net.qqxh.service.task.FileResolveTaskCallBack;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tmatesoft.svn.core.SVNCancelException;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.wc.ISVNEventHandler;
import org.tmatesoft.svn.core.wc.SVNEvent;
import org.tmatesoft.svn.core.wc.SVNEventAction;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by jason on 2019/2/14 10:44
 *
 * @author jason
 */

public class SvnUpdateHandler implements ISVNEventHandler {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private FileResolveTask fileResolveTask;
    private SearchLib searchLib;
    private FileResolveTaskCallBack fileResolveTaskCallBack;
    private Set<File> toResolveList = new HashSet<>();
    private WorkSuccessCallBack workSuccessCallBack;
    private IOFileFilter fileFilter;
    public interface WorkSuccessCallBack {
        void callback();
    }

    public SvnUpdateHandler(WorkSuccessCallBack callBack) {
        this.workSuccessCallBack = callBack;
    }

    @Override
    public void handleEvent(SVNEvent svnEvent, double v) throws SVNException {
        SVNEventAction action = svnEvent.getAction();
        logger.info("update success ：" + action.toString() + svnEvent.getFile().getPath());
        File file = new File(svnEvent.getFile().getPath());
        toResolveList.add(file);
        if (action.getID() == SVNEventAction.UPDATE_COMPLETED.getID()) {
            for (File f : toResolveList) {
                if (f.isFile()&&fileFilter.accept(f)) {
                    Jfile j = new Jfile(f);
                    try {
                        fileResolveTask.doFileResolve(searchLib, j, fileResolveTaskCallBack, 1,1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            workSuccessCallBack.callback();
        }

    }


    public SearchLib getSearchLib() {
        return searchLib;
    }

    public void setSearchLib(SearchLib searchLib) {
        this.searchLib = searchLib;
    }


    public FileResolveTaskCallBack getFileResolveTaskCallBack() {
        return fileResolveTaskCallBack;
    }

    public void setFileResolveTaskCallBack(FileResolveTaskCallBack fileResolveTaskCallBack) {
        this.fileResolveTaskCallBack = fileResolveTaskCallBack;
    }

    @Override
    public void checkCancelled() throws SVNCancelException {

    }

    public void setFileResolveTask(FileResolveTask fileResolveTask) {
        this.fileResolveTask = fileResolveTask;
    }

    public IOFileFilter getFileFilter() {
        return fileFilter;
    }

    public void setFileFilter(IOFileFilter fileFilter) {
        this.fileFilter = fileFilter;
    }
}
