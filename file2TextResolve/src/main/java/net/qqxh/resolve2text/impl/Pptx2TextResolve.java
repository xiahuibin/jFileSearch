package net.qqxh.resolve2text.impl;


import net.qqxh.resolve2text.File2TextResolve;
import org.apache.poi.xslf.extractor.XSLFPowerPointExtractor;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class Pptx2TextResolve implements File2TextResolve {
    private static String TYPE = "pptx";

    @Override
    public String resolve(File file) throws IOException {
        InputStream is = null;
        XMLSlideShow slide = null;
        XSLFPowerPointExtractor extractor = null;
        String text = "";
        try {
            is = new FileInputStream(file);
            slide = new XMLSlideShow(is);
            extractor = new XSLFPowerPointExtractor(slide);
            text = extractor.getText();
        } catch (Exception e) {
            throw e;
        } finally {
            if (extractor != null) {
                try {
                    extractor.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (slide != null) {
                try {
                    slide.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return text;
    }

    @Override
    public String getType() {
        return TYPE;
    }
}
